# use tabs for indentation!

SSH-USER = ash
SSH-HOST = 138.197.124.106
REMOTE-PATH = /var/www/weather.ashleybuttonow.com/html/

deploy:
	rsync -cavze ssh --delete --exclude=.well-known ./_site/ $(SSH-USER)@$(SSH-HOST):$(REMOTE-PATH)

install:
	bundle install
	npm install
	bower install
	composer install

ssh:
	ssh $(SSH-USER)@$(SSH-HOST)
